vitalia - recruitment job
=======

Project is built using Symfony 2.8 and PHP 7.0, documentation was generated using
phpDocumentor script, tested on Apache 2.4 and build in PHP server.

Installation
-------

1 `git clone git@bitbucket.org:toffiak/vitalia.git`

2 `composer install`

3 `app/console doctrine:database:create`

4 `app/console doctrine:schema:update -f`

5 `app/console doctrine:fixtures:load`

6 `app/console server:start`

7 Open in browser URL **http://127.0.0.1:8000**

Run tests
------
bin/phpunit -c app/

Documentation
-----
Available in: **src/AppBundle/Resources/documentation/phpdoc/**




