<?php

declare(strict_types = 1);

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RemoveCartProductType
 *
 * @package AppBundle\Form\Type
 */
class RemoveCartProductType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($options['cart'] as $item) {
            $builder->add($item['product']->getId(), SubmitType::class, [
                'label' => 'global.delete',
                'attr' => ['data' => 'product-id=' . $item['product']->getId()]
            ]);
        }
        $builder->setMethod('DELETE');
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'cart' => null,
            'allow_extra_fields' => true
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getBlockPrefix()
    {
        return 'app_cart_item_delete';
    }
}
