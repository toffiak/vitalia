<?php

declare(strict_types = 1);

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * ProductRepository
 *
 * @package AppBundle\Repository
 */
class ProductRepository extends EntityRepository
{
    /**
     * Returning query used for fetching all existing products,
     * for performance reason it also returning associated categories.
     *
     * @return Query
     */
    public function findAllProductsQuery(): Query
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        return $qb->addSelect('partial p.{id,name,netPrice,grossPrice,identifier}, partial c.{id,name}')
            ->from('AppBundle:Product', 'p')
            ->leftJoin('p.categories', 'c')
            ->getQuery();
    }
}
