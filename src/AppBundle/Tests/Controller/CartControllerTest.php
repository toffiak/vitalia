<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class CartControllerTest
 *
 * @package AppBundle\Tests\Controller
 */
class CartControllerTest extends WebTestCase
{
    public function testShowCart()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/cart');
        $this->assertSame(301, $client->getResponse()->getStatusCode());
        $crawler = $client->request('GET', '/cart/');
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('table.cart')->count());
    }

    public function testAddProduct()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/product');
        $this->assertSame(200, $client->getResponse()->getStatusCode());

        /* Grab first product and add it to cart */
        $link = $crawler->filter('table.products .add-to-cart-link')->eq(0)->link();
        $crawler = $client->click($link);
        $this->assertSame(302, $client->getResponse()->getStatusCode());
    }

    public function testRemoveProduct()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/product');
        $this->assertSame(200, $client->getResponse()->getStatusCode());

        $client->request('POST', '/cart/remove/product', ['_method' => 'DELETE']);
        $this->assertSame(302, $client->getResponse()->getStatusCode());
    }
}
