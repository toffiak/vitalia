<?php

namespace AppBundle\Tests\Model\Cart;

use AppBundle\Model\Cart\CartManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class CartManagerTest
 *
 * @package AppBundle\Tests\Model\Cart
 */
class CartManagerTest extends KernelTestCase
{
    /**
     * @var null|CartManagerInterface
     */
    public $cartManager = null;

    public function setUp()
    {
        self::bootKernel();
        $this->cartManager = static::$kernel->getContainer()->get('app.cart_manager');
    }

    public function testLoadCart()
    {
        $this->assertInstanceOf('\\AppBundle\\Model\\Cart\\Cart', $this->cartManager->loadCart());
    }

    public function testCreateCart()
    {
        $this->assertInstanceOf('\\AppBundle\\Model\\Cart\\Cart', $this->cartManager->createCart());
    }

    public function testSaveCart()
    {
        $cart = $this->getMockBuilder('\\AppBundle\\Model\\Cart\\Cart')->getMock();
        $this->assertTrue($this->cartManager->saveCart($cart));
    }
}
