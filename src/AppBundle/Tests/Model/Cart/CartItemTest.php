<?php

namespace AppBundle\Tests\Model\Cart;

use AppBundle\Model\Cart\CartItem;
use PHPUnit\Framework\TestCase;

/**
 * Class CartItemTest
 *
 * @package AppBundle\Tests\Model\Cart
 */
class CartItemTest extends TestCase
{
    public function testCreating()
    {
        $product = $this->getMockBuilder('\\AppBundle\\Entity\\Product')->getMock();
        $cartItem = new CartItem($product);
        $this->assertInstanceOf('\\AppBundle\\Model\\Cart\\CartItem', $cartItem);
    }

    public function testGetProduct()
    {
        $product = $this->getMockBuilder('\\AppBundle\\Entity\\Product')->getMock();
        $cartItem = new CartItem($product);
        $this->assertSame($product, $cartItem->getProduct());
        $this->assertSame($product, $cartItem['product']);
    }

    public function testGetAmount()
    {
        $product = $this->getMockBuilder('\\AppBundle\\Entity\\Product')->getMock();
        $cartItem = new CartItem($product);
        $this->assertSame(1, $cartItem->getAmount());
        $this->assertSame(1, $cartItem['amount']);
        $cartItem2 = new CartItem($product, 5);
        $this->assertSame(5, $cartItem2->getAmount());
        $this->assertSame(5, $cartItem2['amount']);
    }
}
