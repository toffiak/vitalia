<?php

namespace AppBundle\Tests\Model\Cart\Storage;

use AppBundle\Model\Cart\Storage\CartStorageInterface;
use AppBundle\Model\Cart\Storage\SessionCartStorage;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class SessionCartStorageTest
 *
 * @package AppBundle\Tests\Model\Cart\Storage
 */
class SessionCartStorageTest extends KernelTestCase
{
    /**
     * @var null|CartStorageInterface
     */
    public $sessionCartStorage = null;

    public function setUp()
    {
        self::bootKernel();
        $this->sessionCartStorage = new SessionCartStorage();
        $this->sessionCartStorage->setParameters(['session' => static::$kernel->getContainer()->get('session')]);
    }

    public function testReadStorageData()
    {
        $this->assertSame([], $this->sessionCartStorage->readCartData());
    }

    public function testWriteStorageData()
    {
        $data1 = [['identifier' => 25, 'amount' => 2]];
        $this->sessionCartStorage->writeCartData($data1);
        $this->assertSame($data1, $this->sessionCartStorage->readCartData());
        $data2 = [['identifier2' => 25, 'amount' => 2], ['identifier' => 23, 'amount' => 10]];
        $this->sessionCartStorage->writeCartData($data2);
        $this->assertSame($data2, $this->sessionCartStorage->readCartData());
    }
}
