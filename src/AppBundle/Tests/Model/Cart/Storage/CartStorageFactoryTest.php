<?php

namespace AppBundle\Tests\Model\Cart\Storage;

use AppBundle\Model\Cart\Storage\CartStorageFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class CartStorageFactoryTest
 *
 * @package AppBundle\Tests\Model\Cart\Storage
 */
class CartStorageFactoryTest extends KernelTestCase
{
    /**
     * @var null|CartStorageFactory
     */
    public $cartStorageFactory = null;

    public function setUp()
    {
        self::bootKernel();
        $this->cartStorageFactory = static::$kernel->getContainer()->get('app.cart_storage_factory');
    }

    public function testCreateCartStorageException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->cartStorageFactory->createCartStorage('redis');
    }

    public function testCreateSessionCartStorage()
    {
        $this->assertInstanceOf('\\AppBundle\\Model\\Cart\\Storage\\CartStorageInterface', $this->cartStorageFactory->createCartStorage('Session'));
        $this->assertInstanceOf('\\AppBundle\\Model\\Cart\\Storage\\CartStorageInterface', $this->cartStorageFactory->createCartStorage('sESSion'));
    }
}
