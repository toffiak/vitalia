<?php

namespace AppBundle\Tests\Model\Cart;

use AppBundle\Model\Cart\Cart;
use PHPUnit\Framework\TestCase;

/**
 * Class CartTest
 *
 * @package AppBundle\Tests\Model\Cart
 */
class CartTest extends TestCase
{
    /**
     * @var null|Cart
     */
    public $cart = null;

    public function setUp()
    {
        parent::setUp();
        $this->cart = new Cart();
    }

    public function testAddProduct()
    {
        $this->assertTrue($this->cart->addProduct($this->getMockBuilder('AppBundle\\Entity\\Product')->getMock()));
        $this->assertSame(1, count($this->cart));
    }

    public function testAddCartItem()
    {
        /* Add first cart item */
        $product1 = $this->getMockBuilder('\\AppBundle\\Entity\\Product')->getMock();
        $product1->method('getIdentifier')->willReturn(3);
        $cartItem1 = $this->getMockBuilder('\\AppBundle\\Model\\Cart\\CartItem')->setConstructorArgs([$product1])->getMock();
        $cartItem1->method('offsetGet')->will($this->returnValueMap([['product', $product1], ['amount', 3]]));
        $this->assertTrue($this->cart->addCartItem($cartItem1));
        $this->assertSame(1, count($this->cart));

        /* Add second one cart item */
        $product2 = $this->getMockBuilder('\\AppBundle\\Entity\\Product')->getMock();
        $product2->method('getIdentifier')->willReturn(7);
        $cartItem2 = $this->getMockBuilder('\\AppBundle\\Model\\Cart\\CartItem')->setConstructorArgs([$product2, 5])->getMock();
        $cartItem2->method('offsetGet')->willReturn($product2);
        $cartItem1->method('offsetGet')->will($this->returnValueMap([['product', $product2], ['amount', 3]]));
        $this->assertTrue($this->cart->addCartItem($cartItem2));
        $this->assertSame(2, count($this->cart));
    }

    public function testRemoveCartItem()
    {
        $product = $this->getMockBuilder('\\AppBundle\\Entity\\Product')->getMock();
        $product->method('getIdentifier')->willReturn(25);
        $cartItem = $this->getMockBuilder('\\AppBundle\\Model\\Cart\\CartItem')->setConstructorArgs([$product])->getMock();
        $nonExistingCartItem = $this->getMockBuilder('\\AppBundle\\Model\\Cart\\CartItem')->setConstructorArgs([$product])->getMock();

        /* Add cart item */
        $this->assertTrue($this->cart->addCartItem($cartItem));
        $this->assertSame(1, count($this->cart));

        /* Remove non existing one */
        $this->assertFalse($this->cart->removeCartItem($nonExistingCartItem));
        $this->assertSame(1, count($this->cart));

        /* Remove previously added cart item */
        $this->assertTrue($this->cart->removeCartItem($cartItem));
        $this->assertSame(0, count($this->cart));
    }

    public function testFindCartItemByProduct()
    {
        $product = $this->getMockBuilder('\\AppBundle\\Entity\\Product')->getMock();
        $product->method('getIdentifier')->willReturn(100);
        $cartItem = $this->getMockBuilder('\\AppBundle\\Model\\Cart\\CartItem')->setConstructorArgs([$product])->getMock();
        $cartItem->method('offsetGet')->will($this->returnValueMap([['product', $product]]));
        $nonExistingProductInCart = $this->getMockBuilder('\\AppBundle\\Entity\\Product')->getMock();
        $nonExistingProductInCart->method('getIdentifier')->willReturn(21);

        /* Add cart item */
        $this->assertTrue($this->cart->addCartItem($cartItem));
        $this->assertSame(1, count($this->cart));

        /* Check if cart item with product exists in cart */
        $this->assertNull($this->cart->findCartItemByProduct($nonExistingProductInCart));
        $this->assertSame($cartItem, $this->cart->findCartItemByProduct($product));
    }
}
