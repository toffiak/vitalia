<?php

declare(strict_types = 1);

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadCategoryData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /* Electronics */
        $electronicsCategory = new Category();
        $electronicsCategory->setName('Electronics');

        /* Books */
        $booksCategory = new Category();
        $booksCategory->setName('Books');

        $manager->persist($electronicsCategory);
        $manager->persist($booksCategory);
        $manager->flush();

        $this->addReference('electronics-category', $electronicsCategory);
        $this->addReference('books-category', $booksCategory);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
