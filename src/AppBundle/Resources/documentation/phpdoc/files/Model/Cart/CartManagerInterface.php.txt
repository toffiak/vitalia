<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart;

/**
 * Class CartManagerInterface
 *
 * @package AppBundle\Model\Cart
 */
interface CartManagerInterface
{
    /**
     * Loading cart from storage or returning new empty cart
     *
     * @return Cart
     */
    public function loadCart(): Cart;

    /**
     * Saving cart
     *
     * @param Cart $cart
     * @return bool
     */
    public function saveCart(Cart $cart): bool;

    /**
     * Creating cart
     *
     * @return Cart
     */
    public function createCart(): Cart;
}
