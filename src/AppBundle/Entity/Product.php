<?php

declare(strict_types = 1);

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Product
 *
 * @package AppBundle\Entity
 */
class Product
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $netPrice;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var float
     */
    private $grossPrice;

    /**
     * @var VATRate
     */
    private $VATRate;

    /**
     * @var Collection
     */
    private $categories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->identifier = uniqid("", true);
    }

    /**
     * Returning human readable product.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getName();
    }

    /**
     * Get id
     *
     * @return null|int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName(string $name): Product
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set netPrice
     *
     * @param float $netPrice
     * @return Product
     */
    public function setNetPrice(float $netPrice): Product
    {
        $this->netPrice = $netPrice;

        return $this;
    }

    /**
     * Get netPrice
     *
     * @return float
     */
    public function getNetPrice(): float
    {
        return (float)$this->netPrice;
    }

    /**
     * Set grossPrice
     *
     * @param float $grossPrice
     * @return Product
     */
    public function setGrossPrice(float $grossPrice): Product
    {
        $this->grossPrice = $grossPrice;

        return $this;
    }

    /**
     * Get grossPrice
     *
     * @return float
     */
    public function getGrossPrice(): float
    {
        return (float)$this->grossPrice;
    }

    /**
     * Set VATRate
     *
     * @param VATRate $vATRate
     * @return Product
     */
    public function setVATRate(VATRate $vATRate = null): Product
    {
        $this->VATRate = $vATRate;

        return $this;
    }

    /**
     * Get VATRate
     *
     * @return null|VATRate
     */
    public function getVATRate()
    {
        return $this->VATRate;
    }

    /**
     * Add categories
     *
     * @param Category $categories
     * @return Product
     */
    public function addCategory(Category $categories): Product
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param Category $categories
     */
    public function removeCategory(Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return Product
     */
    public function setIdentifier($identifier): Product
    {
        $this->identifier = $identifier;

        return $this;
    }
}
