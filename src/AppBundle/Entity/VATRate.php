<?php

declare(strict_types = 1);

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class VATRate
 *
 * @package AppBundle\Entity
 */
class VATRate
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $value;

    /**
     * Returning human readable VAT rate.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getValue() . '%';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param int $value
     * @return VATRate
     */
    public function setValue(int $value): VATRate
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
