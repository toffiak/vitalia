<?php

declare(strict_types = 1);

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class HomepageController
 *
 * @package AppBundle\Controller
 */
class HomepageController extends Controller
{
    /**
     * Homepage.
     *
     * @return RedirectResponse
     */
    public function homepageAction()
    {
        return $this->redirectToRoute('app_product_list');
    }
}