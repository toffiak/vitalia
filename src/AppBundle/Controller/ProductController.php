<?php

declare(strict_types = 1);

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProductController
 *
 * @package AppBundle\Controller
 */
class ProductController extends Controller
{
    /**
     * List of products
     *
     * @param $page
     * @return Response
     */
    public function listAction(int $page)
    {
        $em = $this->getDoctrine()->getManager();
        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:Product')->findAllProductsQuery(),
            $page,
            $this->getParameter('nb_of_items_on_list')
        );

        return $this->render('AppBundle:Product:list.html.twig', ['pagination' => $pagination]);
    }
}
