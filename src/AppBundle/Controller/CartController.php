<?php

declare(strict_types = 1);

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Form\Type\DeleteProductType;
use AppBundle\Form\Type\RemoveCartProductType;
use AppBundle\Model\Cart\Sorter\SortByVATRates;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CartController
 *
 * @package AppBundle\Controller
 */
class CartController extends Controller
{
    /**
     * Adding product to cart.
     *
     * @param Product $product
     * @return RedirectResponse
     */
    public function addProductAction(Product $product)
    {
        $cartManager = $this->get('app.cart_manager');
        $cart = $cartManager->loadCart();
        $cart->addProduct($product);
        $cartManager->saveCart($cart);

        return $this->redirectToRoute('app_product_list');
    }

    /**
     * Removing product from cart.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function removeProductAction(Request $request)
    {
        if (!$request->isMethod('DELETE')) {
            return $this->redirectToRoute('app_product_list');
        }
        $removeCartProductForm = $this->createForm(RemoveCartProductType::class, null, ['cart' => []]);
        $removeCartProductForm->handleRequest($request);
        if ($removeCartProductForm->isSubmitted() && $removeCartProductForm->isValid()) {
            $this->processRemoveProduct($removeCartProductForm);
        }

        return $this->redirectToRoute('app_product_list');
    }

    /**
     * Showing cart.
     *
     * @return Response
     */
    public function showCartAction()
    {
        $cart = $this->get('app.cart_manager')->loadCart();
        $sorted = $cart->getSorted(new SortByVATRates());
        $removeCartProductForm = $this->createForm(RemoveCartProductType::class, null, ['cart' => $cart]);

        return $this->render('AppBundle:Cart:show.html.twig', ['cart' => $sorted, 'remove_cart_product_form' => $removeCartProductForm->createView()]);
    }

    /**
     * Processing deleting product from cart
     *
     * @param FormInterface $removeCartProductForm
     */
    private function processRemoveProduct(FormInterface $removeCartProductForm)
    {
        $em = $this->getDoctrine()->getManager();
        $cartManager = $this->get('app.cart_manager');
        try {
            $cart = $cartManager->loadCart();
            foreach ($removeCartProductForm->getExtraData() as $productId => $v) {
                $cartItem = $cart->findCartItemByProduct($em->getRepository('AppBundle:Product')->find($productId));
                $cart->removeCartItem($cartItem);
                $cartManager->saveCart($cart);
            }
            $em->flush();
        } catch (\Exception $e) {
            $this->get('logger')->error($e->getMessage());
        }
    }
}