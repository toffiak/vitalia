<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart;

/**
 * Interface CartInterface
 *
 * @package AppBundle\Model\Cart
 */
interface CartInterface
{
    /**
     * Adding cart item to cart.
     * 
     * @param CartItem $cartItem
     * @return bool
     */
    public function addCartItem(CartItem $cartItem): bool;

    /**
     * Removing cart item from cart.
     *
     * @param CartItem $cartItem
     * @return bool
     * @return mixed
     */
    public function removeCartItem(CartItem $cartItem): bool;
}