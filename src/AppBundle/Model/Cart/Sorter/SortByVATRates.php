<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart\Sorter;

use AppBundle\Model\Cart\CartInterface;

/**
 * Class SortByVATRates
 *
 * Sorting cart items by VAT rates
 *
 * @package AppBundle\Model\Cart\Sorter
 */
class SortByVATRates implements SorterInterface
{
    /**
     * {@inheritDoc}
     */
    public function sort(CartInterface $cart)
    {
        $sorted = [];
        foreach ($cart as $item) {
            $sorted[$item['product']->getVATRate()->getValue()][] = $item;
        }

        /* Sort from highest VAT rate to lowest one */
        krsort($sorted);

        return $sorted;
    }
}