<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart\Sorter;

use AppBundle\Model\Cart\CartInterface;

/**
 * Interface SorterInterface
 * 
 * @package AppBundle\Model\Cart\Sorter
 */
interface SorterInterface
{
    /**
     * Sorting cart with a selected sorting method.
     *
     * @param CartInterface $cart
     * @return mixed
     */
    public function sort(CartInterface $cart);
}