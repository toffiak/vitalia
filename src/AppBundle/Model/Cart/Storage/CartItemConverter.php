<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart\Storage;

use AppBundle\Model\Cart\CartItem;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CartItemConverter
 *
 * Converting cart item given as an array into real CartItem object or
 * converting CartItem object into array.
 *
 * @package AppBundle\Model\Cart\Storage
 */
class CartItemConverter implements CartItemConverterInterface
{
    /**
     * @var null|EntityManagerInterface
     */
    private $entityManager = null;

    /**
     * Set entity manager
     *
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     */
    public function fromArray(array $cartItemData)
    {
        $product = $this->entityManager->getRepository('AppBundle:Product')->findOneBy(['identifier' => $cartItemData['identifier']]);
        if (!empty($product)) {
            return new CartItem($product, $cartItemData['amount']);
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(CartItem $item): array
    {
        return [
            'identifier' => $item['product']->getIdentifier(),
            'amount' => $item['amount']
        ];
    }
}