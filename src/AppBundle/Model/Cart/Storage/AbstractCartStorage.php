<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart\Storage;

use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class AbstractCartStorage
 *
 * @package AppBundle\Model\Cart\Storage
 */
abstract class AbstractCartStorage
{
    /**
     * @var null|ParameterBag
     */
    protected $cartParameters = null;

    /**
     * Setting parameters required for selected cart storage
     *
     * @param array $parameters
     */
    public function setParameters(array $parameters = [])
    {
        $this->cartParameters = new ParameterBag($parameters);
    }
}