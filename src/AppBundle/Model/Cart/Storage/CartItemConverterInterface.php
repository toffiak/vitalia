<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart\Storage;

use AppBundle\Model\Cart\CartItem;

/**
 * Class CartItemConverterInterface
 *
 * @package AppBundle\Model\Cart\Storage
 */
interface CartItemConverterInterface
{
    /**
     * Converting array into CartItem object
     *
     * @param array $cartItemData
     * @return CartItem|null
     */
    public function fromArray(array $cartItemData);

    /**
     * Converting CartItem object into array
     *
     * @param CartItem $cartItem
     * @return array
     */
    public function toArray(CartItem $cartItem): array;
}