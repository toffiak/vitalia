<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart\Storage;

/**
 * Interface CartStorageInterface
 * 
 * Cart storage is a place where cart data is stored, it can be almost anything:
 * session, redis, flat file etc.
 *
 * @package AppBundle\Model\Cart\Storage
 */
interface CartStorageInterface
{
    /**
     * Reading and returning cart data from cart storage.
     * 
     * @return array
     */
    public function readCartData(): array;

    /**
     * Writing cart data to cart storage
     * 
     * @param array $cartData
     * @return bool
     */
    public function writeCartData(array $cartData): bool;
}