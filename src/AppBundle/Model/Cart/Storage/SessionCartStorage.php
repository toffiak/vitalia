<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart\Storage;

/**
 * Class SessionCartStorage
 *
 * Storing cart data into session
 *
 * @package AppBundle\Model\Cart\Storage
 */
class SessionCartStorage extends AbstractCartStorage implements CartStorageInterface
{
    /**
     * {@inheritDoc}
     */
    public function readCartData(): array
    {
        return $this->cartParameters->get('session')->get('cart', []);
    }

    /**
     * @inheritDoc
     */
    public function writeCartData(array $cartData): bool
    {
        $session = $this->cartParameters->get('session');
        $session->set('cart', $cartData);

        return true;
    }
}