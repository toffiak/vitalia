<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart\Storage;

use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class CartStorageFactory
 *
 * @package AppBundle\Model\Cart\Storage
 */
class CartStorageFactory
{
    /**
     * @var Session
     */
    private $session;

    /**
     * Creating cart storage by name.
     *
     * Currently only "session" storage is available.
     *
     * @param $storageName
     * @throws \InvalidArgumentException
     * @return CartStorageInterface
     */
    public function createCartStorage($storageName): CartStorageInterface
    {
        switch (mb_strtolower($storageName)) {
            case 'session':
                $sessionCartStorage = new SessionCartStorage();
                $sessionCartStorage->setParameters(['session' => $this->session]);
                return $sessionCartStorage;
            default:
                throw new \InvalidArgumentException(sprintf('Cart storage "%s" was not found', $storageName));
        }
    }

    /**
     * Set session
     *
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->session = $session;
    }
}