<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart;

use AppBundle\Entity\Product;

/**
 * Class CartItem
 *
 * @package AppBundle\Model\Cart
 */
class CartItem implements \ArrayAccess
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var int
     */
    private $amount = 0;

    /**
     * CartItem constructor.
     *
     * @param Product $product
     * @param int $amount
     */
    public function __construct(Product $product, $amount = 1)
    {
        $this->product = $product;
        $this->amount = $amount;
    }

    /**
     * Returning product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Returning cart item amount
     *
     * @return int
     */
    public function getAmount():int
    {
        return $this->amount;
    }

    /**
     * {@inheritDoc}
     */
    public function offsetExists($offset): bool
    {
        return isset($this->{$offset});
    }

    /**
     * {@inheritDoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->{$offset} = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function offsetUnset($offset)
    {
        unset($this->{$offset});
    }

    /**
     * {@inheritDoc}
     */
    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset)) {
            throw new \ErrorException(\sprintf('Property "%s" does not exists in "' . __CLASS__ . '" class', $offset));
        }

        return $this->{$offset};
    }
}
