<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart;

use AppBundle\Entity\Product;
use AppBundle\Model\Cart\Sorter\SorterInterface;

/**
 * Class Cart
 *
 * @package AppBundle\Model\Cart
 */
class Cart implements CartInterface, \Countable, \IteratorAggregate
{
    /**
     * @var array
     */
    private $items = [];

    /**
     * Helper method, used for adding cart item without creating CartItem object directly.
     * Under the hood it construct valid CartItem object and adding it to itself.
     *
     * @param Product $product
     * @return bool
     */
    public function addProduct(Product $product): bool
    {
        return $this->addCartItem(new CartItem($product));
    }

    /**
     * Finding cart item by a given Product entity.
     *
     * @param Product $product
     * @return null|CartItem
     */
    public function findCartItemByProduct(Product $product)
    {
        $matchedCartItems = array_filter($this->items, function ($item) use ($product) {
            return $item['product']->getIdentifier() === $product->getIdentifier() ? true : false;
        });

        return !empty($matchedCartItems) ? array_pop($matchedCartItems) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function addCartItem(CartItem $cartItem): bool
    {
        $itemExists = false;
        foreach ($this->items as $item) {
            if ($item['product']->getIdentifier() === $cartItem['product']->getIdentifier()) {
                $itemExists = true;
                $item['amount'] = $item['amount'] + 1;
            }
        }

        if (false === $itemExists) {
            array_push($this->items, $cartItem);
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function removeCartItem(CartItem $cartItem): bool
    {
        $itemRemoved = false;
        foreach ($this->items as $k => $item) {
            if ($item === $cartItem) {
                unset($this->items[$k]);
                $itemRemoved = true;
            }
        }

        return $itemRemoved;
    }

    /**
     * {@inheritDoc}
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator(): \Iterator
    {
        return new \ArrayIterator($this->items);
    }

    /**
     * Sorting cart items with a selected method.
     *
     * @param SorterInterface|null $sorter
     * @return array
     */
    public function getSorted(SorterInterface $sorter = null): array
    {
        return null !== $sorter ? $sorter->sort($this) : $this->items;
    }
}