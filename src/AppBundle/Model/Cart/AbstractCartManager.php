<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart;

use AppBundle\Model\Cart\Storage\CartItemConverterInterface;
use AppBundle\Model\Cart\Storage\CartStorageInterface;

/**
 * Class AbstractCartManager
 *
 * @package AppBundle\Model\Cart
 */
abstract class AbstractCartManager
{
    /**
     * @var null|CartStorageInterface
     */
    protected $storage = null;

    /**
     * @var null|CartItemConverterInterface
     */
    protected $cartItemConverter = null;

    /**
     * Setting cart storage
     *
     * @param CartStorageInterface $storage
     */
    public function setCartStorage(CartStorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Setting cart item converter
     *
     * @param CartItemConverterInterface $cartItemConverter
     */
    public function setCartItemConverter(CartItemConverterInterface $cartItemConverter)
    {
        $this->cartItemConverter = $cartItemConverter;
    }
}