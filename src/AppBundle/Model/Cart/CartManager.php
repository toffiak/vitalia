<?php

declare(strict_types = 1);

namespace AppBundle\Model\Cart;

/**
 * Class CartManager
 *
 * @package AppBundle\Model\Cart
 */
class CartManager extends AbstractCartManager implements CartManagerInterface
{
    /**
     * {@inheritDoc}
     */
    public function loadCart(): Cart
    {
        $cartData = $this->storage->readCartData();
        $cart = $this->createCart();
        foreach ($cartData as $cartItemData) {
            $cartItem = $this->cartItemConverter->fromArray($cartItemData);
            if (null !== $cartItem) {
                $cart->addCartItem($cartItem);
            }
        }

        return $cart;
    }

    /**
     * {@inheritDoc}
     */
    public function saveCart(Cart $cart): bool
    {
        $cartData = [];
        if (0 !== count($cart)) {
            foreach ($cart as $cartItem) {
                $cartData[] = $this->cartItemConverter->toArray($cartItem);

            }
        }
        
        return $this->storage->writeCartData($cartData);
    }

    /**
     * {@inheritDoc}
     */
    public function createCart(): Cart
    {
        return new Cart();
    }
}