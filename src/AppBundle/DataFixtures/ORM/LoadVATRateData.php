<?php

declare(strict_types = 1);

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\VATRate;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadVATRateData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadVATRateData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /* 23% VAT */
        $VATRate23 = new VATRate();
        $VATRate23->setValue(23);

        /* 8% VAT */
        $VATRate8 = new VATRate();
        $VATRate8->setValue(8);

        $manager->persist($VATRate23);
        $manager->persist($VATRate8);
        $manager->flush();

        $this->addReference('VAT-rate-23', $VATRate23);
        $this->addReference('VAT-rate-8', $VATRate8);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
}