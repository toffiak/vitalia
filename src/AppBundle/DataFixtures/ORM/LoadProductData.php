<?php

declare(strict_types = 1);

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadProductData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /* i3-530 */
        $i3530 = new Product();
        $i3530->setName('i3-530');
        $i3530->setNetPrice(223.66);
        $i3530->setGrossPrice(275.10);
        $i3530->setVATRate($this->getReference('VAT-rate-23'));
        $i3530->addCategory($this->getReference('electronics-category'));

        /* i3-2130 */
        $i32130 = new Product();
        $i32130->setName('i3-2130');
        $i32130->setNetPrice(275.00);
        $i32130->setGrossPrice(338.25);
        $i32130->setVATRate($this->getReference('VAT-rate-23'));
        $i32130->addCategory($this->getReference('electronics-category'));

        /* i3-6098 */
        $i36098 = new Product();
        $i36098->setName('i3-6098');
        $i36098->setNetPrice(400.99);
        $i36098->setGrossPrice(493.22);
        $i36098->setVATRate($this->getReference('VAT-rate-23'));
        $i36098->addCategory($this->getReference('electronics-category'));

        /* "Gone with the wild" */
        $goneWithTheWildBook = new Product();
        $goneWithTheWildBook->setName('Gone with the wild');
        $goneWithTheWildBook->setNetPrice(59.86);
        $goneWithTheWildBook->setGrossPrice(73.63);
        $goneWithTheWildBook->setVATRate($this->getReference('VAT-rate-8'));
        $goneWithTheWildBook->addCategory($this->getReference('books-category'));

        /* "The pillars of the earth" */
        $thePillarsOfTheEarthBook = new Product();
        $thePillarsOfTheEarthBook->setName('The pillars of the earth');
        $thePillarsOfTheEarthBook->setNetPrice(35.00);
        $thePillarsOfTheEarthBook->setGrossPrice(43.05);
        $thePillarsOfTheEarthBook->setVATRate($this->getReference('VAT-rate-8'));
        $thePillarsOfTheEarthBook->addCategory($this->getReference('books-category'));

        /* RX540 */
        $RX540 = new Product();
        $RX540->setName('RX580');
        $RX540->setNetPrice(199);
        $RX540->setGrossPrice(244.77);
        $RX540->setVATRate($this->getReference('VAT-rate-23'));
        $RX540->addCategory($this->getReference('electronics-category'));
        $RX540->addCategory($this->getReference('books-category'));

        /* "The thorn birds" */
        $theThornBirdsBook = new Product();
        $theThornBirdsBook->setName('The thorn birds');
        $theThornBirdsBook->setNetPrice(83.23);
        $theThornBirdsBook->setGrossPrice(102.37);
        $theThornBirdsBook->setVATRate($this->getReference('VAT-rate-8'));
        $theThornBirdsBook->addCategory($this->getReference('books-category'));

        $manager->persist($i3530);
        $manager->persist($i32130);
        $manager->persist($i36098);
        $manager->persist($goneWithTheWildBook);
        $manager->persist($thePillarsOfTheEarthBook);
        $manager->persist($RX540);
        $manager->persist($theThornBirdsBook);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}